let num = parseInt(prompt("Enter a number:"))

console.log(num)

for (let i = num; i > 0; i--){
	if(i <= 50) {
		console.log("Less than 50. Breaking loop.")
		break;
	}
	if (i % 10 == 0) {
		console.log("Divisible by 10, skipping iteration")
		continue;
	}
	if (i % 5 == 0) {
		console.log("Current number " + i)
	}
}

let longString = "supercalifragilisticexpialidocious"
let consonantString = ""


for (let i = 0; i < longString.length; i++) {
	if(longString[i] === 'a' || longString[i] === 'e' || longString[i] === 'i' || longString[i] === 'o' || longString[i] === 'u') {
		continue
	}
	else {
		consonantString += longString[i]
	}
}

console.log("Consonant String is " + consonantString)